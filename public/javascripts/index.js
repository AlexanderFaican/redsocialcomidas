
$(document).ready(function(){
    $('.materialboxed').materialbox();
    $('.collapsible').collapsible();
    $('.slider').slider();
    $('.carousel').carousel();
    
    var  estado_menu_top = 0;
    $('#btn_menu_top').click(function(){
        if(estado_menu_top == 0){
            //Cambiar esto del boton
            $('#btn_menu_top').removeClass('fa-arrow-circle-down');
            $('#btn_menu_top').addClass('fa-arrow-alt-circle-up');
            //Cambiar esto del menu y nav
            $('#login_div').addClass('active');
            $('#navbar').addClass('active');
            estado_menu_top = 1;
        }else if(estado_menu_top == 1){
            $('#btn_menu_top').removeClass('fa-arrow-alt-circle-up');
            $('#btn_menu_top').addClass('fa-arrow-circle-down');
            $('#login_div').removeClass('active');
            $('#navbar').removeClass('active');
            estado_menu_top = 0;
        }
    });

    $('#btn_DefaultPage').click(function(){
        location.href = '/';
    });

    //Registro de receta /begin -----------------------------
    

    $('#add_ing').click(function(){
        let nombre = $('#ing_nombre').val();
        let cantidad = $('#ing_cantidad').val();
        
        if(nombre.length > 0 && cantidad.length > 0) {
            let new_ing = {
                pos: (lista_ingredientes_receta.length > 0) ? lista_ingredientes_receta.length : 0,
                nombre: nombre,
                cantidad: cantidad
            };
 
            let html  = '<tr id="ing_at_'+new_ing.pos+'">';
                html += '   <td>'+new_ing.nombre+'</td>';
                html += '   <td>'+new_ing.cantidad+'</td>';
                html += '   <td class="icono"><i class="fas fa-trash-alt delete_item_ing" data-target="'+new_ing.pos+'"></i></td>';
                html += '</tr>';
            lista_ingredientes_receta.push(new_ing);
            $('#table_ingBody').append(html);

            $('#ing_nombre').val('');
            $('#ing_cantidad').val('');
        }else{
            M.toast({html: 'Error, existen campos incompletos'})
        }
    });

    $('body').on('click', '.delete_item_ing', function(){
        let pos = '#ing_at_'+$(this).attr('data-target');
        $(pos).remove();
        
        let actual = parseInt($(this).attr('data-target'));
        lista_ingredientes_receta.forEach(function(ingrediente, i){
            if(ingrediente.pos === actual){
                lista_ingredientes_receta.splice(i, 1);
            }
        });
    });

    $('#add_ins').click(function(){
        let instruccion = $('#ins_item').val();
        
        if(instruccion.length > 0) {
            let new_ins = {
                pos: (lista_instrucciones_receta.length > 0) ? lista_instrucciones_receta.length : 0,
                instruccion: instruccion
            };
 
            let html  = '<tr id="ins_at_'+new_ins.pos+'">';
                html += '   <td>'+new_ins.instruccion+'</td>';
                html += '   <td class="icono"><i class="fas fa-trash-alt delete_item_ins" data-target="'+new_ins.pos+'"></i></td>';
                html += '</tr>';
            lista_instrucciones_receta.push(new_ins);
            $('#table_insBody').append(html);

            $('#ins_item').val('');
        }else{
            M.toast({html: 'Error, existen campos incompletos'})
        }
    });

    $('body').on('click', '.delete_item_ins', function(){
        let pos = '#ins_at_'+$(this).attr('data-target');
        $(pos).remove();

        let actual = parseInt($(this).attr('data-target'));
        lista_instrucciones_receta.forEach(function(instruccion, i){
            if(instruccion.pos === actual){
                lista_instrucciones_receta.splice(i, 1);
            }
        });
    });

    $('#add_vid').click(function(){
        let video = $('#vid_item').val();
        
        if(video.length > 0) {
            let new_video = {
                pos: (lista_videos_receta.length > 0) ? lista_videos_receta.length : 0,
                video: video
            };
 
            let html  = '<tr id="vid_at_'+new_video.pos+'">';
                html += '   <td>'+new_video.video+'</td>';
                html += '   <td class="icono"><i class="fas fa-trash-alt delete_item_vid" data-target="'+new_video.pos+'"></i></td>';
                html += '</tr>';
                lista_videos_receta.push(new_video);
            $('#table_vidBody').append(html);

            $('#vid_item').val('');
        }else{
            M.toast({html: 'Error, existen campos incompletos'})
        }
    });

    $('body').on('click', '.delete_item_vid', function(){
        let pos = '#vid_at_'+$(this).attr('data-target');
        $(pos).remove();

        let actual = parseInt($(this).attr('data-target'));
        lista_videos_receta.forEach(function(video, i){
            if(video.pos === actual){
                lista_videos_receta.splice(i, 1);
            }
        });
    });

    $('.btn_addphoto').click(function(){
        let target = $(this).attr('data-target');
        $('#'+target).click();
    });

    $('#input_a').change(function(){
        mostrarPreviewInBackGroundImagen(this);
    });

    $('#input_b').change(function(){
        mostrarPreviewInBackGroundImagen(this);
    });

    $('#input_c').change(function(){
        mostrarPreviewInBackGroundImagen(this);
    });
    
    function mostrarPreviewInBackGroundImagen(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                let id = '#'+input.id+'_preview';
                $(''+id).css('background-image', 'url('+e.target.result+')');
                $(''+id).css('background-size', '100% 100%');
                $(''+id).css('background-repeat', 'no-repeat');
            }
        }
    }

    $('#guardar_receta').click(function(){
        var formData = new FormData(); 

        //Get fotos 
        let file_a = $('#input_a')[0].files[0];
        let file_b = $('#input_b')[0].files[0];
        let file_c = $('#input_c')[0].files[0];

        formData.append('pais', $('#pais').val());
        formData.append('provincia', $('#provincia').val());
        formData.append('canton', $('#canton').val());
        formData.append('nombre', $('#nombre').val());
        formData.append('apodo', $('#apodo').val());
        formData.append('foto_a', file_a);
        formData.append('foto_b', file_b);
        formData.append('foto_c', file_c);
        formData.append('videos', JSON.stringify(lista_videos_receta));
        formData.append('ingredientes', JSON.stringify(lista_ingredientes_receta));
        formData.append('instrucciones', JSON.stringify(lista_instrucciones_receta));
        //Son para actualizar
        formData.append('external_id_receta_reg', $('#external_id_receta_reg').val());
        formData.append('id_receta', $('#id_receta').val());
        formData.append('foto_a_url', $('#foto_a_url').val());
        formData.append('foto_b_url', $('#foto_b_url').val());
        formData.append('foto_c_url', $('#foto_c_url').val());

        if(lista_ingredientes_receta.length > 0 && lista_instrucciones_receta.length > 0 && lista_videos_receta.length > 0){
            if ($('#registrar_receta').find('input:text').val().length > 0) {
                if($('#external_id_receta_reg').val() != '0' || ($('#input_a')[0].files[0] && $('#input_b')[0].files[0] && $('#input_c')[0].files[0])) {
                        $.ajax({
                            url: '/users/registrarReceta',
                            type:"POST",
                            data: formData,
                            contentType: false,
                            processData: false,
                        }).done(function (data) {
                            M.toast({html: data.data});
                            if (data.code == 0) {
                                $('#registrar_receta').find('input:text').val('');
                                $('#table_ingBody').empty();
                                $('#table_insBody').empty();
                                $('#table_vidBody').empty();
                                lista_ingredientes_receta = [];
                                lista_instrucciones_receta = [];
                                lista_videos_receta = [];
                                $('#input_a').val(null);
                                $('#input_b').val(null);
                                $('#input_b').val(null);
                                $('#input_a_preview').css('background-image', 'none');
                                $('#input_b_preview').css('background-image', 'none');
                                $('#input_c_preview').css('background-image', 'none');
                                location.href = '/';
                            }
                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            M.toast({html: 'Error, no se pudo completar la operación'});
                        });
                } else{
                    M.toast({html: 'Error, existen campos incompletos'});
                }
            }else{
                M.toast({html: 'Error, existen campos incompletos'});
            }
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
        
    });

    $('.cerrar_modal').click(function(){
        $('#'+$(this).attr('data-target')).removeClass('active');

        if($(this).attr('data-target') === 'modal_addreceta'){
            $('#registrar_receta').find('input:text').val('');
            $('#table_ingBody').empty();
            $('#table_insBody').empty();
            $('#table_vidBody').empty();
            lista_ingredientes_receta = [];
            lista_instrucciones_receta = [];
            lista_videos_receta = [];
            $('#input_a').val(null);
            $('#input_b').val(null);
            $('#input_b').val(null);
            $('#input_a_preview').css('background-image', 'none');
            $('#input_b_preview').css('background-image', 'none');
            $('#input_c_preview').css('background-image', 'none');
        }
    });

    $('#add_receta').click(function(){
        $('#modal_addreceta').addClass('active');
    });

    $('#recetas_in_favoritos').click(function(){
        $('#modal_favoritos').addClass('active');
    });

    $('#show_recetas').click(function(){
        $('#modal_misrecetas').addClass('active');
    });


    //Registro de receta /end   -----------------------------
    $('.addFavorito').click(function(){
        let id_usuario = $('#id_usuario').val();
        let id_receta = $(this).attr('data-target');
        if(id_usuario.length > 0){
            $.ajax({
                url: '/users/registrarFavorito',
                type:"POST",
                data: {
                    id_persona: $('#id_usuario').val(),
                    id_receta: $(this).attr('data-target')
                },
                dataType: 'json',
            }).done(function (data) {
                $('#addfavoritoReceta_'+id_receta).addClass('hide');
                $('#removefavoritoReceta_'+id_receta).removeClass('hide');
                
                M.toast({html: data.data});
                location.href = '/';
            }).fail(function (jqXHR, textStatus, errorThrown) {
                M.toast({html: 'Error, no se pudo completar la operación'});
            });
        }else{
            M.toast({html: 'Debes iniciar sesión '});
        }
    });

    $('.removeFavorito').click(function(){
        let id_usuario = $('#id_usuario').val();
        let id_receta = $(this).attr('data-target');
        if(id_usuario.length > 0){
            $.ajax({
                url: '/users/removeFavorito',
                type:"POST",
                data: {
                    id_persona: $('#id_usuario').val(),
                    id_receta: $(this).attr('data-target')
                },
                dataType: 'json',
            }).done(function (data) {
                M.toast({html: data.data});
                $('#removefavoritoReceta_'+id_receta).addClass('hide');
                $('#addfavoritoReceta_'+id_receta).removeClass('hide');
                location.href = '/';
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                M.toast({html: 'Error, no se pudo completar la operación'});
            });
        }else{
            M.toast({html: 'Debes iniciar sesión '});
        }
    });

    $('.addLikeReceta').click(function(){
        let id_usuario = $('#id_usuario').val();
        let id_receta = $(this).attr('data-target');
        if(id_usuario.length > 0){
            $.ajax({
                url: '/users/registrarLike',
                type:"POST",
                data: {
                    id_persona: id_usuario,
                    id_receta: id_receta
                },
                dataType: 'json',
            }).done(function (data) {
                M.toast({html: data.data});
                $('#addlikeReceta_'+id_receta).addClass('hide');
                $('#removelikeReceta_'+id_receta).removeClass('hide');
                let total = parseInt($('#totalLikesReceta_'+id_receta).text());
                $('#totalLikesReceta_'+id_receta).text((total + 1));
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                M.toast({html: 'Error, no se pudo completar la operación'});
            });
        }else{
            M.toast({html: 'Debes iniciar sesión '});
        }
    });

    $('.removeLikeReceta').click(function(){
        let id_usuario = $('#id_usuario').val();
        let id_receta = $(this).attr('data-target');
        if(id_usuario.length > 0){
            $.ajax({
                url: '/users/removeLike',
                type:"POST",
                data: {
                    id_persona: id_usuario,
                    id_receta: id_receta
                },
                dataType: 'json',
            }).done(function (data) {
                M.toast({html: data.data});
                $('#removelikeReceta_'+id_receta).addClass('hide');
                $('#addlikeReceta_'+id_receta).removeClass('hide');
                let total = parseInt($('#totalLikesReceta_'+id_receta).text());
                $('#totalLikesReceta_'+id_receta).text((total - 1));
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                M.toast({html: 'Error, no se pudo completar la operación'});
            });
        }else{
            M.toast({html: 'Debes iniciar sesión '});
        }
    });

    $('#criterio_busqueda').keypress(function (e) {
        if(e.which ==13){
            let cadena = $(this).val();
            if($('#id_usuario').val().length > 0){
                if(cadena.length > 0) {
                    $('#formbuscar').submit();
                }
            }else{
                M.toast({html: 'Debes iniciar sesión '});
            }
        }
     });

    $('#btn_logout').click(function(){
        location.href = '/logout';
    });

    $('#btn_correo').click(function(){
        $('#modal_login').addClass('active');
    });

    $('.delete_receta').click(function(){
        let id_usuario = $('#id_usuario').val();
        let external_id = $(this).attr('data-target');
        if(id_usuario.length > 0){
            $.ajax({
                url: '/users/removeReceta',
                type:"POST",
                data: {
                    id_persona: id_usuario,
                    external_id: external_id
                },
                dataType: 'json',
            }).done(function (data) {
                M.toast({html: data.data});
                if(data.code === 0){
                    location.reload();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                M.toast({html: 'Error, no se pudo completar la operación'});
            });
        }else{
            M.toast({html: 'Debes iniciar sesión '});
        }
    });
});