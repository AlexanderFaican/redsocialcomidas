require('dotenv').config();
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var LocalStrategy = require('passport-local').Strategy;
var cuenta_modelo = require('./modelos/cuenta');
var persona_modelo = require('./modelos/persona');

/*let persona = new persona_modelo({
    apellidos: 'Admin', 
    nombres: 'User',
    url_foto: '', 
    rol: 1,
});

let cuenta = new cuenta_modelo({
    googleID: '',
    correo: 'admin@redsocialcomidas.com',
    password: 'qwe2123',
});

persona.cuenta = cuenta;
cuenta.persona = persona;

persona.saveAll({cuenta: true}).then(function(persona){
	console.log(persona);
});*/

//Config login with Google
passport.use(new GoogleStrategy({
		clientID: "660659284087-r6gjsngco05mit50cflloo4eajjtmvu0.apps.googleusercontent.com",
		clientSecret: "dgjKGI3WqCzF9roz1fyiTlGg", 
		callbackURL: "http://localhost:3000/auth/google/callback",
	},
	function (accessToken, refreshToken, profile, cb) {
		//Create user in BD or find it
		let user = {
			tipo: 0,
			profile: profile
		};
		return cb(null, user);
	}
));

passport.use(new LocalStrategy(
	function (username, password, cb) {
		let user = {
			tipo: 1,
			profile: []
		};
		return cb(null, user);
	}
));

passport.serializeUser(function (user, cb) {
	cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
	cb(null, obj);
});


//App configuration
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var logger = require('morgan');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var sesionRouter = require('./routes/sesion');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

var hbs = require('hbs');

hbs.registerHelper('json',function(obj) {
	return new hbs.SafeString(JSON.stringify(obj))
});

hbs.registerHelper('comparar', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

app.use(session({
	secret: 'RedSocialRecetas',
	resave: false,
	saveUninitialized: true
}));

app.use(logger('dev'));
app.use(logger('combined'));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/archivos', express.static('archivos'));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/sesion', sesionRouter);

app.get('/auth/google', passport.authenticate('google', {scope: ['profile']}));

app.post('/auth/login', passport.authenticate('local', { failureRedirect: '/' }), function(req, res){
	if(req.user){
		cuenta_modelo.getJoin({persona: true}).filter({correo: req.body.username, password: req.body.password}).then(usuario => {
			req.user.profile = usuario[0];
			res.redirect('/');
		}).catch(error => {
			res.redirect('/');
		});
	}else{
		res.redirect('/');
	}
});

app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/error_auth' }),function(req, res) {
	cuenta_modelo.getJoin({persona: true}).filter({googleID: req.user.profile.id}).run().then(function (data) {
		if (data.length == 0) {
			//El usuario es primera vez que  accede, se procede a registrar
			let cuenta = new cuenta_modelo({
				googleID: req.user.profile.id,
				correo: '',
				password: '',
			});

			let persona = new persona_modelo({
				apellidos: req.user.profile.name.familyName, 
				nombres: req.user.profile.name.givenName, 
				url_foto: req.user.profile.photos[0].value,
				rol: 0, //0 usuario normal, 1 admin
			});

			persona.cuenta = cuenta;
			cuenta.persona = persona;

			persona.recetas = [];
			persona.favoritos = [];

			persona.saveAll({cuenta: true}).then(function (user) {
				console.log(user);
				res.redirect('/');
			}).error(function (error) {
				res.redirect('/');
			});
		}else{
			res.redirect('/');
		}
	});
});

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;