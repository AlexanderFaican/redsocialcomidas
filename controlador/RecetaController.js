'use strict';
var fs = require('fs');

var cuenta_modelo = require('../modelos/cuenta');
var persona_modelo = require('../modelos/persona');
var receta_modelo = require('../modelos/receta');
var ranking_modelo = require('../modelos/ranking');
var ingrediente_modelo = require('../modelos/ingrediente');
var favoritos_modelo = require('../modelos/favoritos');

class Receta_Controller {
    
    registrarReceta(req, res) {
        if(req.user) {
            console.log(req.user);
            if(req.user.tipo == 0) { //Logeado con google
                cuenta_modelo.getJoin({persona: true}).filter({googleID: req.user.profile.id}).then(cuenta => {
                    saveRecetaDb(cuenta[0].persona, req, res);
                }).catch(error => {
                    console.log(error);
                    res.json({code:-1, data:'Error al guardar la receta'});
                });
            } else if(req.user.tipo == 1) { //Logeado con email and password
                saveRecetaDb(req.user.profile.persona, req, res);
            }
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }

    registrarFavorito(req, res) {
        if(req.user) {
            let data = req.body;
            let favorito = new favoritos_modelo({
                id_persona: data.id_persona,
                id_receta: data.id_receta,
            });

            favorito.saveAll().then(function(favorito_saved){
                res.json({code:0, data:'Se ha guardado en favoritos'});
            }).catch(function(error){
                res.json({code:-1, data:'Error al guardar la receta en favoritos'});
            });
            
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }

    removeFavorito(req, res) {
        if(req.user) {
            let data = req.body;
            console.log(data);
            favoritos_modelo.filter({id_persona: data.id_persona, id_receta: data.id_receta}).delete().then(function(){
                res.json({code:0, data:'Se ha eliminado de favoritos'});
            }).catch(function(error){
                console.log(error);
                res.json({code:-1, data:'Error al eliminar la receta de favoritos'});
            });
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }

    registrarLike(req, res) {
        console.log(req.body);
        if(req.user) {
            let like = new ranking_modelo({
                id_receta: req.body.id_receta,
                id_persona: req.body.id_persona,
            });

            like.saveAll().then(function(liked_saved){
                console.log(liked_saved);
                res.json({code:0, data:'Like registrado'});
            }).catch(function(error){
                res.json({code:-1, data:'Error al registrar el like'});
            });
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }

    removeLike(req, res) {
        if(req.user) {
            let data = req.body;
            console.log(data);
            ranking_modelo.filter({id_persona: data.id_persona, id_receta: data.id_receta}).delete().then(function(){
                res.json({code:0, data:'Se ha eliminado el like'});
            }).catch(function(error){
                console.log(error);
                res.json({code:-1, data:'Error al eliminar el like de la receta'});
            });
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }

    removeReceta(req, res) {
        if(req.user) {
            let data = req.body;
            console.log(data);
            receta_modelo.filter({external_id: data.external_id}).delete().then(function(upd){
                res.json({code:0, data: 'La receta fue dada de baja'}); 
            }).catch(function(error){
                res.json({code:-1, data:'Error al dar de baja la receta'});    
            });
        }else{
            res.json({code:-1, data:'Error, no tiene permiso para realizar esta acción'});
        }
    }
    
}
module.exports = Receta_Controller;

function saveRecetaDb(persona, req, res) {
    let data = req.body;
    if(data.external_id_receta_reg == '0'){
        //Receta Object
        let receta = new receta_modelo({
            id_persona: persona.id,
            pais: data.pais,
            provincia: data.provincia,
            canton: data.canton,
            nombre: data.nombre,
            apodo: data.apodo,
            fotoa: '',
            fotob: '',
            fotoc: '',
            videos: [],
            instrucciones: [],
            estado: 0, //0 activa, 1 deshabilitada
        });

        receta.persona = persona;

        //Lista de ingredientes
        let lista_ingredientes = [];
        JSON.parse(data.ingredientes).forEach(function(i){
            let ingrediente = new ingrediente_modelo({
                nombre: i.nombre,
                cantidad: i.cantidad,
            });
            ingrediente.receta = receta;
            lista_ingredientes.push(ingrediente);
        });
        receta.ingredientes = lista_ingredientes;

        //Lista de instrucciones
        JSON.parse(data.instrucciones).forEach(function(ins){
            receta.instrucciones.push(ins.instruccion);
        });

        //Lista de videos
        JSON.parse(data.videos).forEach(function(v){
            receta.videos.push(v.video);
        });

        receta.saveAll({ingredientes: true}).then(function(receta_saved){
            //Guardar las fotos
            //Obtener ruta de los archivos
            var tmp_path_1 = req.files['foto_a'][0].path;
            var tmp_path_2 = req.files['foto_b'][0].path;
            var tmp_path_3 = req.files['foto_c'][0].path;

            //Ruta de donde se van a guardar
            var target_path_1 = 'archivos/'+receta_saved.id+'-foto_a.jpg';
            var target_path_2 = 'archivos/'+receta_saved.id+'-foto_b.jpg';
            var target_path_3 = 'archivos/'+receta_saved.id+'-foto_c.jpg';

            //Copiar los archivos a la carpeta de destino
            coyFile(tmp_path_1, target_path_1, function(estado_1){
                if(estado_1)
                    coyFile(tmp_path_2, target_path_2, function(estado_2){
                        if(estado_2)
                            coyFile(tmp_path_3, target_path_3, function(estado_3){
                                if(estado_3){
                                    receta_modelo.get(receta_saved.id).update({fotoa:target_path_1, fotob:target_path_2, fotoc:target_path_3,}).then(function(receta_update){
                                        console.log(receta_update);
                                        res.json({code:0, data:'Receta Guardada'});
                                    }).catch(function(error){
                                        console.log(error);
                                        res.json({code:-1, data:'Error al guardar la receta'});
                                    });
                                }else
                                    res.json({code:-1, data:'Error al guardar la receta'});
                            });
                        else
                            res.json({code:-1, data:'Error al guardar la receta'});
                    });
                else
                    res.json({code:-1, data:'Error al guardar la receta'});
            });
        }).catch(function(error){
            console.log(error);
            res.json({code:-1, data:'Error al guardar la receta'});
        });
    } else {
        let receta = new receta_modelo({
            id: data.id_receta,
            external_id: data.external_id_receta_reg,
            id_persona: persona.id,
            pais: data.pais,
            provincia: data.provincia,
            canton: data.canton,
            nombre: data.nombre,
            apodo: data.apodo,
            fotoa: '',
            fotob: '',
            fotoc: '',
            videos: [],
            instrucciones: [],
            estado: 0, //0 activa, 1 deshabilitada
        });

        receta.persona = persona;

        //Lista de ingredientes
        let lista_ingredientes = [];
        JSON.parse(data.ingredientes).forEach(function(i){
            let ingrediente = new ingrediente_modelo({
                nombre: i.nombre,
                cantidad: i.cantidad,
            });
            ingrediente.receta = receta;
            lista_ingredientes.push(ingrediente);
        });
        receta.ingredientes = lista_ingredientes;

        //Lista de instrucciones
        JSON.parse(data.instrucciones).forEach(function(ins){
            receta.instrucciones.push(ins.instruccion);
        });

        //Lista de videos
        JSON.parse(data.videos).forEach(function(v){
            receta.videos.push(v.video);
        });

        ingrediente_modelo.filter({id_receta: data.id_receta}).delete().then(function(deleted){
            savedFilesRecursivo(req, data, {foto_a:'', foto_b:'', foto_c:''}, 0, function(urls){
                receta.fotoa = (urls.foto_a.length > 0) ? urls.foto_a : data.foto_a_url;
                receta.fotob = (urls.foto_b.length > 0) ? urls.foto_b : data.foto_b_url;
                receta.fotoc = (urls.foto_c.length > 0) ? urls.foto_c : data.foto_c_url;

                receta.setSaved();
                
                receta.saveAll({ingredientes: true}).then(function(receta_saved){
                    console.log(receta_saved);
                    res.json({code:0, data:'Receta Actualizada'});
                }).catch(function(error){
                    console.log(error);
                    res.json({code:-1, data:'Error al actualizar la receta'});
                });
            });
        }).catch(function(error){
            console.log(error);
            res.json({code:-1, data:'Error al actualizar la receta'});
        });

    }
}

function savedFilesRecursivo(req, data, urls, contador, callback){
    if(contador == 0) {
        if(req.files['foto_a']){
            let tmp_path_1 = req.files['foto_a'][0].path;
            let target_path_1 = 'archivos/'+data.id_receta+'-foto_a.jpg';
            coyFile(tmp_path_1, target_path_1, function(estado_1){
                if(estado_1){
                    urls.foto_a = target_path_1;
                }
                contador = contador + 1;
                savedFilesRecursivo(req, data, urls, contador, callback);    
            });
        }else{
            contador = contador + 1;
            savedFilesRecursivo(req, data, urls, contador, callback);
        }
    } else if(contador == 1) {
        if(req.files['foto_b']){
            let tmp_path_2 = req.files['foto_b'][0].path;
            let target_path_2 = 'archivos/'+data.id_receta+'-foto_b.jpg';
            coyFile(tmp_path_2, target_path_2, function(estado_2){
                if(estado_2){
                    urls.foto_b = target_path_2;
                }
                contador = contador + 1;
                savedFilesRecursivo(req, data, urls, contador, callback);    
            });
        }else{
            contador = contador + 1;
            savedFilesRecursivo(req, data, urls, contador, callback);
        }
    } else if(contador == 2){
        if(req.files['foto_c']){
            let tmp_path_3 = req.files['foto_c'][0].path;
            let target_path_3 = 'archivos/'+data.id_receta+'-foto_c.jpg';
            coyFile(tmp_path_3, target_path_3, function(estado_3){
                if(estado_3){
                    urls.foto_c = target_path_3;
                }
                contador = contador + 1;
                savedFilesRecursivo(req, data, urls, contador, callback);    
            });
        }else{
            contador = contador + 1;
            savedFilesRecursivo(req, data, urls, contador, callback);
        }
    } else if(contador == 3) {
        callback(urls);
    }
}

function coyFile(tmp_path, target_path, callback){
    var src = fs.createReadStream(tmp_path);
    var dest = fs.createWriteStream(target_path);
    src.pipe(dest);
    src.on('end', function(){
       callback(true);
    });
    src.on('error', function(err) { 
        callback(false);
    }); 
}