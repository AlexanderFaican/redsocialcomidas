var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Ingrediente = thinky.createModel("Ingrediente", {
    id: type.string(),
    external_id: type.string().default(r.uuid()), 
    nombre: type.string(),
    cantidad: type.string(),
    createdAt: type.date().default(r.now()),
    id_receta: type.string()
});

module.exports = Ingrediente;

var Receta = require('./receta');
Ingrediente.belongsTo(Receta, "receta", "id_receta", "id");






