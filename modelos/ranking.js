var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Ranking = thinky.createModel("Ranking", {
    id: type.string(),
    external_id: type.string().default(r.uuid()), 
    id_receta: type.string(),
    id_persona: type.string(),
});

module.exports = Ranking;

var Persona = require('./persona');
Ranking.belongsTo(Persona, "persona", "id_persona", "id");

var Receta = require('./receta');
Ranking.belongsTo(Receta, "receta", "id_receta", "id");


