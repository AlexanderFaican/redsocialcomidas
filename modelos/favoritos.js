var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Favoritos = thinky.createModel("Favoritos", {
    id: type.string(),
    external_id: type.string().default(r.uuid()), 
    createdAt: type.date().default(r.now()),
    id_persona: type.string(),
    id_receta: type.string(),
});

module.exports = Favoritos;

var Persona = require('./persona');
Favoritos.belongsTo(Persona, "persona", "id_persona", "id");

var Receta = require('./receta');
Favoritos.belongsTo(Receta, "receta", "id_receta", "id");






