var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Receta = thinky.createModel("Receta", {
    id: type.string(),
    external_id: type.string().default(r.uuid()), 
    id_persona: type.string(),
    //Datos de ubicacion
    pais: type.string(),
    provincia: type.string(),
    canton: type.string(),
    //Datos de identificacion
    nombre: type.string(),
    apodo: type.string(),
    //Datos multimedia
    fotoa: type.string(),
    fotob: type.string(),
    fotoc: type.string(),
    videos: type.array(),
    //Ingredientes
    //Instrucciones de preparación
    instrucciones: type.array(),
    estado: type.number(),
});

module.exports = Receta;

var Persona = require('./persona');
Receta.belongsTo(Persona, "persona", "id_persona", "id");

var Ingrediente = require('./ingrediente');
Receta.hasMany(Ingrediente, "ingredientes", "id", "id_receta");

var Ranking = require('./ranking');
Receta.hasMany(Ranking, "ranking", "id", "id_receta");


