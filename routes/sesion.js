var express = require('express');

//Login y demas
var cuenta_controler = require('../controlador/CuentaController');
var cuenta = new cuenta_controler();

var router = express.Router();

router.post('/login', cuenta.iniciar_sesion);
router.get('/logout', cuenta.cerrar_sesion);

module.exports = router;
