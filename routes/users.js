var express = require('express');
var multer  = require('multer');
var upload = multer({ dest: 'tmp_uploads/' });

var receta_control  = require('../controlador/RecetaController');
var receta_obj = new receta_control();
var router = express.Router();

/* Recetas */
var imgupload = upload.fields([{ name: 'foto_a', maxCount: 1 }, { name: 'foto_b', maxCount: 1 }, { name: 'foto_c', maxCount: 1 }]);
router.post('/registrarReceta', imgupload, receta_obj.registrarReceta);

router.post('/registrarFavorito', receta_obj.registrarFavorito);
router.post('/removeFavorito', receta_obj.removeFavorito);
router.post('/registrarLike', receta_obj.registrarLike);
router.post('/removeLike', receta_obj.removeLike);
router.post('/removeReceta', receta_obj.removeReceta);

module.exports = router;
