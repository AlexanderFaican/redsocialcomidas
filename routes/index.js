var express = require('express');
var receta_modelo = require('../modelos/receta');
var cuenta_modelo = require('../modelos/cuenta');
var persona_modelo = require('../modelos/persona');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
	if(req.user){
		if(req.user.tipo == 0) {
			redirectoWithGoogleLogin(req,res);
		}else if(req.user.tipo == 1){
			redirectWithLoginNormal(req,res);
		}
	}else{
		noLoginSendRecetas(req,res);
	}
});

function noLoginSendRecetas(req,res) {
	receta_modelo.getJoin({ingredientes: true, ranking: true}).filter({estado: 0}).orderBy('createdAt').then(function(recetas){
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: recetas
			}
		});
	}).catch(function(error){
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: [],
			}
		});
	});
}

function redirectoWithGoogleLogin(req,res){
	receta_modelo.getJoin({ingredientes: true, ranking: true}).filter({estado: 0}).orderBy('createdAt').then(function(recetas){
		cuenta_modelo.getJoin({persona: true}).filter({googleID: req.user.profile.id}).run().then(function (cuenta) {
			persona_modelo.getJoin({cuenta: true, recetas: {ingredientes: true}, favoritos: {receta: {ingredientes: true, ranking: true}}}).filter({external_id: cuenta[0].persona.external_id}).run().then(function(persona){
				console.log(persona[0]);
				/*persona[0].recetas.forEach(function(receta){
					receta.ingredientes = JSON.stringify(receta.ingredientes);
					receta.instrucciones = JSON.stringify(receta.instrucciones);
					receta.videos = JSON.stringify(receta.videos);
				});*/
				res.render('index', {
					title: 'Comida Típica',
					data: {
						dot_log: true,
						user: persona[0],
						lista_recetas: recetas,
					}
				});	
			}).catch(function(error){
				console.log(error);
				res.render('index', {
					title: 'Comida Típica',
					data: {
						dot_log: false,
						lista_recetas: recetas
					}
				});
			});
		}).catch(function(error){
			console.log(error);
			res.render('index', {
				title: 'Comida Típica',
				data: {
					dot_log: false,
					lista_recetas: recetas
				}
			});
		});
	}).catch(function(error){
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: [],
			}
		});
	});
}

function redirectWithLoginNormal(req,res){
	receta_modelo.getJoin({ingredientes: true, ranking: true}).filter({estado: 0}).orderBy('createdAt').then(function(recetas){
		persona_modelo.getJoin({cuenta: true, recetas: {ingredientes: true}, favoritos: {receta: {ingredientes: true, ranking: true}}}).filter({external_id: req.user.profile.persona.external_id}).run().then(function(persona){
			res.render('index', {
				title: 'Comida Típica',
				data: {
					dot_log: true,
					user: persona[0],
					lista_recetas: recetas
				}
			});	
		}).catch(function(error){
			res.render('index', {
				title: 'Comida Típica',
				data: {
					dot_log: false,
					lista_recetas: recetas
				}
			});
		});
	}).catch(function(error){
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: [],
			}
		});
	});
}

router.get('/error_auth', function (req, res, next) {
	res.render('index', {
		title: 'Comida Típica',
		data: {
			code: -1,
			message: 'Error al iniciar sesión con Google',
			loggedin: false
		}
	});
});

router.post('/buscaReceta', function(req, res) {
	let data = req.body.criteriobusqueda;
	if(req.user) {
		receta_modelo.getJoin({ingredientes: true, ranking: true}).filter(function(rec){
			return rec('provincia').eq(data).or(rec('canton').eq(data));
		}).then(function(recetas){
			if(req.user.tipo == 0) {
				redirectoWithGoogleLoginBeta(recetas, req,res);
			}else if(req.user.tipo == 1){
				redirectWithLoginNormalBeta(recetas, req,res);
			}
		}).catch(function(error){
			console.log(error);
			res.render('index', {
				title: 'Comida Típica',
				data: {
					dot_log: false,
					lista_recetas: recetas
				}
			});
		});
	}else{
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: []
			}
		});
	}
});

function redirectoWithGoogleLoginBeta(recetas, req,res){
    console.log(recetas);
	cuenta_modelo.getJoin({persona: true}).filter({googleID: req.user.profile.id}).run().then(function (cuenta) {
		persona_modelo.getJoin({cuenta: true, recetas: {ingredientes: true}, favoritos: {receta: {ingredientes: true, ranking: true}}}).filter({external_id: cuenta[0].persona.external_id}).run().then(function(persona){
			console.log(persona[0]);
			persona[0].recetas.forEach(function(receta){
				receta.ingredientes = JSON.stringify(receta.ingredientes);
				receta.instrucciones = JSON.stringify(receta.instrucciones);
				receta.videos = JSON.stringify(receta.videos);
				receta.fotos = JSON.stringify(receta.fotos);
			});
			res.render('index', {
				title: 'Comida Típica',
				data: {
					code: 0,
					dot_log: true,
					user: persona[0],
					lista_recetas: recetas,
				}
			});	
		}).catch(function(error){
			console.log(error);
			res.render('index', {
				title: 'Comida Típica',
				data: {
					dot_log: false,
					lista_recetas: recetas
				}
			});
		});
	}).catch(function(error){
		console.log(error);
		res.render('index', {
			title: 'Comida Típica',
			data: {
				dot_log: false,
				lista_recetas: recetas
			}
		});
	});
}

function redirectWithLoginNormalBeta(recetas, req,res){
    persona_modelo.getJoin({cuenta: true, recetas: {ingredientes: true}, favoritos: {receta: {ingredientes: true, ranking: true}}}).filter({external_id: req.user.profile.persona.external_id}).run().then(function(persona){
        console.log(persona[0]);
        persona[0].recetas.forEach(function(receta){
            receta.ingredientes = JSON.stringify(receta.ingredientes);
            receta.instrucciones = JSON.stringify(receta.instrucciones);
            receta.videos = JSON.stringify(receta.videos);
            receta.fotos = JSON.stringify(receta.fotos);
        });
        res.render('index', {
            title: 'Comida Típica',
            data: {
                code: 0,
                dot_log: true,
                user: persona[0],
                lista_recetas: recetas,
            }
        });	
    }).catch(function(error){
        console.log(error);
        res.render('index', {
            title: 'Comida Típica',
            data: {
                dot_log: false,
                lista_recetas: recetas
            }
        });
    });
}

module.exports = router;